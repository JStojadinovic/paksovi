class AddConnectedToPacsses < ActiveRecord::Migration[5.1]
  def change
    add_column :pacsses, :connected, :string
  end
end
