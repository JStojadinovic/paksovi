class CreatePacsses < ActiveRecord::Migration[5.1]
  def change
    create_table :pacsses do |t|
      t.string :pacs_id
      t.string :title
      t.string :owner
      t.string :ppmc
      t.string :status
      t.date :last_updated

      t.timestamps
    end
  end
end
