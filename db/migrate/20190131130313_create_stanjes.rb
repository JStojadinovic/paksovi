class CreateStanjes < ActiveRecord::Migration[5.1]
  def change
    create_table :stanjes do |t|
      t.text :stanje
      t.references :pacss, foreign_key: true

      t.timestamps
    end
  end
end
