require 'test_helper'

class PacssesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pacss = pacsses(:one)
  end

  test "should get index" do
    get pacsses_url
    assert_response :success
  end

  test "should get new" do
    get new_pacss_url
    assert_response :success
  end

  test "should create pacss" do
    assert_difference('Pacss.count') do
      post pacsses_url, params: { pacss: { last_updated: @pacss.last_updated, owner: @pacss.owner, pacs_id: @pacss.pacs_id, ppmc: @pacss.ppmc, status: @pacss.status, title: @pacss.title } }
    end

    assert_redirected_to pacss_url(Pacss.last)
  end

  test "should show pacss" do
    get pacss_url(@pacss)
    assert_response :success
  end

  test "should get edit" do
    get edit_pacss_url(@pacss)
    assert_response :success
  end

  test "should update pacss" do
    patch pacss_url(@pacss), params: { pacss: { last_updated: @pacss.last_updated, owner: @pacss.owner, pacs_id: @pacss.pacs_id, ppmc: @pacss.ppmc, status: @pacss.status, title: @pacss.title } }
    assert_redirected_to pacss_url(@pacss)
  end

  test "should destroy pacss" do
    assert_difference('Pacss.count', -1) do
      delete pacss_url(@pacss)
    end

    assert_redirected_to pacsses_url
  end
end
