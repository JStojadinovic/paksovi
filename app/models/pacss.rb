class Pacss < ApplicationRecord
    validates :pacs_id, presence: true, length: { is: 12 }, uniqueness: true
    validates :ppmc, length: { maximum: 5 }
    validates :title, presence: true
    validates :owner, presence: true
    has_many :comments, dependent: :destroy
    has_many :stanjes
    
    def self.search(search)
        where("pacs_id LIKE ? OR title LIKE ? ", "%#{search}%", "%#{search}%")
    end

    def self.search_by_stat(search)
        if search == 'Samo_otvoreni'
            where("status NOT LIKE ?", "%Closed%") 
        else
            where("status LIKE ? or status LIKE ?", "%WithCustomer%", "%Pending Client Confirmation%")
        end
    end
end
