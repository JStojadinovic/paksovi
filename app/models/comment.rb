class Comment < ApplicationRecord
  belongs_to :pacss
  validates :body, length: {minimum: 10 }
  after_save :update_pacss_time
  
  def update_pacss_time
    self.pacss.updated_at = Time.now
    self.pacss.save
  end
  
  def self.search(search)
    where("body LIKE ?", "%#{search}%")
  end
end
