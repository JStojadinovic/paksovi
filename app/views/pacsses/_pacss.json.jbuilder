json.extract! pacss, :id, :pacs_id, :title, :owner, :ppmc, :status, :last_updated, :created_at, :updated_at
json.url pacss_url(pacss, format: :json)
