class CommentsController < ApplicationController
    before_action :set_pacss
    
    def create
        @pacss.comments.create! comments_params
        redirect_to @pacss
    end
    
    private
        def set_pacss
            @pacss = Pacss.find(params[:pacss_id])
        end
        
        def comments_params
            params.required(:comment).permit(:body, :uneo)
        end
end
