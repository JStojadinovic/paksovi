class SessionsController < ApplicationController
  def new
  end

  def create
    redirect_to root_path
    
    user = User.find_by(username: params[:session][:username])
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
    else 
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path
  end
end
