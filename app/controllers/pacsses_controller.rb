class PacssesController < ApplicationController
  before_action :set_pacss, only: [:show, :edit, :update, :destroy]

  # GET /pacsses
  # GET /pacsses.json
  def index
    @pacsses = Pacss.all
    if params[:search]
      @pacsses_1 = @pacsses.search(params[:search])
      @pacsses.each do |pacss|
        pacss.comments.each do |comment|
          if comment.body.include?(params[:search])
            begin
              @pacsses_1 = @pacsses_1 + @pacsses.where(id: comment.pacss_id)
            rescue
              @pacsses_1 = @pacsses.where(id: comment.pacss_id)
            end
          end
				end
			end
			@pacsses = @pacsses_1.uniq
    elsif params[:status]
      @pacsses = @pacsses.search_by_stat(params[:status])
    elsif params[:by]
      @pacsses = Pacss.where(owner: params[:by])
    end
    begin
      @pacsses = @pacsses.order(:updated_at).reverse.uniq
    rescue
    end
  end

  # GET /pacsses/1
  # GET /pacsses/1.json
  def show
    # @pacsses = Pacss.all
  end

  # GET /pacsses/new
  def new
    @pacss = Pacss.new
  end

  # GET /pacsses/1/edit
  def edit
  end

  # POST /pacsses
  # POST /pacsses.json
  def create
    @pacss = Pacss.new(pacss_params)

    respond_to do |format|
      if @pacss.save
        @status = Stanje.new
        @status.pacss_id = @pacss.id
        @status.stanje = @pacss.status
        @status.save
        format.html { redirect_to @pacss, notice: 'Pacss was successfully created.' }
        format.json { render :show, status: :created, location: @pacss }
      else
        format.html { render :new }
        format.json { render json: @pacss.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pacsses/1
  # PATCH/PUT /pacsses/1.json
  def update
    respond_to do |format|
      if @pacss.update(pacss_params)
        @status = Stanje.new
        @status.pacss_id = @pacss.id
        @status.stanje = @pacss.status
        @status.save
        format.html { redirect_to @pacss, notice: 'Pacss was successfully updated.' }
        format.json { render :show, status: :ok, location: @pacss }
      else
        format.html { render :edit }
        format.json { render json: @pacss.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pacsses/1
  # DELETE /pacsses/1.json
  def destroy
    @pacss.destroy
    respond_to do |format|
      format.html { redirect_to pacsses_url, notice: 'Pacss was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pacss
      @pacss = Pacss.find(params[:id])
      @stanja = Stanje.where(pacss_id: params[:id]).order(created_at: :DESC)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pacss_params
      params.require(:pacss).permit(:pacs_id, :title, :owner, :ppmc, :status, :last_updated, :connected, :banka, :routines)
    end
end
